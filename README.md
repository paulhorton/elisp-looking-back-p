# elisp-looking-back-p

Provides function `looking-back-p' to complete the pattern

    looking-back    looking-at
    looking-back-p  looking-at-p

The other three being included in emacs.

In the spirit of feeping creaturism;
looking predicates returning the matching string when true are also provided:

    looking-at?
    looking-bk?  alias of looking-back?

and a "looking at something other than REGEX" predicate:

    looking-at-not?


See commentary in looking.el for details.
