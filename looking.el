;;; looking.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2022, 2023 Paul Horton

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20220720
;; Updated: 20231127
;; Version: 0.0
;; Keywords: regular expression searching

;; License: GPLv3

;;; Commentary:

;;  Provides function `looking-back-p' to complete the pattern
;;
;;    looking-back    looking-at
;;    looking-back-p  looking-at-p
;;
;;  The other three being included in emacs.
;;
;;  In the spirit of feeping creaturism;
;;  looking predicates returning the matching string when true are also provided:
;;
;;    * looking-at?
;;    * looking-bk?  alias of looking-back?
;;
;;  and a "looking at something other than REGEX" predicate:
;;
;;    * looking-at-not?
;;
;;
;;  Caveat:
;;  By the nature of the problem, the looking-back functions are too slow in some cases.
;;  But regexs with few alterations are often fast enough.
;;
;;
;;  To consider:
;;  It would be interesting to consider "back-looking regular expressions"
;;  which match the reverse of the buffer text.
;;
;;  So for example, consider the situation
;;
;;       tempvar=<point>      ;; with point at <point>
;;
;;  (looking-back "\\([[:alpha:]]+\\)=$")
;;  (match-string 0) --> "r"
;;
;;  But a hypothetical back-looking regex "([[:alpha:]]+\\)="
;;
;;  (back-looking "\\([[:alpha:]]+\\)=$")
;;  (match-string 0) --> "varname"
;;
;;  Would be a big project but perhaps useful.

;;; Change Log:

;;; Code:


(require 'let1)
(require 'shortcuts-math-symbols-basic)


;; I could grab this defalias from shortcuts.el
;; but that would pull in a lot of stuff.
(defalias 'match-beg  'match-beginning)


(defvar looking-back-p/limit-default
  1024
  "Default limit of how far back before point to search."
  )


(cl-defun looking-back-p
    (regexp &optional
            (limit-pos (- (point) looking-back-p/limit-default))
            greedy
            )
  "Similar to `looking-back' except:
* Does not change the match data.
* Provides default value for LIMIT-POS"
  (let ((inhibit-changing-match-data t))
    (looking-back regexp limit-pos greedy)
    ))


(cl-defun looking-back?
    (regex &key
           (limit-pos (- (point) looking-back-p/limit-default))
           greedy
           props
           )
  "Similar to `looking-back', but returns the match string upon success.
Uses keyword arguments.
When PROPS is true, returned string includes any string properties."
  (save-excursion
    (and
     (looking-back regex limit-pos greedy)
     (if props
         (match-string 0)
       (match-string-no-properties 0)
       )
    )))


(defun looking-at? (regex)
  "Like `looking-at-p', but returns matched string when true."
  (save-match-data
    (when (looking-at regex)
      (match-string 0)
      )))



(defun looking-at-not? (regex)
  "Return true if there is text following (point),
that does *not* match the regular expression REGEX.

In other words, looking-at-something-other-than-regex?"
  (if (eobp)
      nil
    (not (looking-at-p regex))
    ))


(defalias 'looking-bk? 'looking-back?
  "For visual symmetry with looking-at?"
  )


(cl-defun looking-on (regex &optional
                            (pos  (point))
                            (limit-beg  (max (point-min) (- pos looking-back-p/limit-default)))
                            (limit-end  (point-max)))
    "Is position POS on a match to REGEX?
Match must be contained within LIMIT-BEG LIMIT-END,
and should be on either side of POS.

In other words, if when point is placed on POS
either char-before or char-after would be part of
the match, then POS is considered \"on\" the match.

When true, the entire match string is return.
Group match data can be extracted from the match data."
    (save-excursion
      (goto-char limit-beg)
      (while (≦ limit-beg (point))
        (ifnot (re-search-forward regex limit-end t)
            (cl-return-from looking-on nil)
          (unless (< (match-beg 0) (match-end 0))
            (error "looking-on confused by regex matching the empty string."))
          (when (≦ (match-beg 0) pos (match-end 0))
            (cl-return-from looking-on (match-string 0))
            )))))


(cl-defun looking-on/goto-beg (regex &optional
                                     (pos  (point))
                                     (limit-beg  (max (point-min) (- pos looking-back-p/limit-default)))
                                     (limit-end  (point-max)))
  "If POS is `looking-on' REGEX, goto the beg of the match;
and return the matching string,
otherwise do nothing and return nil"
  (when-let1 match (looking-on regex pos limit-beg limit-end)
    (goto-char (match-beg 0))
    match
    ))

(cl-defun looking-on/goto-end (regex &optional
                                     (pos  (point))
                                     (limit-beg  (max (point-min) (- pos looking-back-p/limit-default)))
                                     (limit-end  (point-max)))
  "If POS is `looking-on' REGEX, goto the end of the match;
and return the matching string,
otherwise do nothing and return nil"
  (when-let1 match (looking-on regex pos limit-beg limit-end)
    (goto-char (match-end 0))
    match
    ))



(provide 'looking)

;;; looking.el ends here
